function urlencode(str)
  if (str) then
    str = string.gsub (str, "\n", "\r\n")
    str = string.gsub (str, "([^%w .-_])",
    function (c) return string.format ("%%%02X", string.byte(c)) end)
    str = string.gsub (str, " ", "+")
  end
  return str    
end

math.randomseed(util.time())
local offset = math.random(0,5251) * 1000
local response = http.request_batch({{"GET", "virtuoso-for-trustypublishing-test.inn.ac/sparql?default-graph-uri=&query=select+%3Fnp+where+{%3Fnp+a+<http%3A%2F%2Fwww.nanopub.org%2Fnschema%23Nanopublication>}+LIMIT+1000+OFFSET+" .. offset .. "&format=text%2Fcsv&timeout=0", response_body_bytes = 10000000}})
for l in string.gmatch(response[1].body, "%S+") do
  if l ~= '"np"' and math.random() < 0.1 then
    local npUrl = string.sub(l, 2, -2)
    local encodedNpUrl = urlencode(npUrl)
    local getUrl = "http://virtuoso-for-trustypublishing-test.inn.ac/sparql?default-graph-uri=&query=prefix+np%3A+%3Chttp%3A%2F%2Fwww.nanopub.org%2Fnschema%23%3E%0D%0Aprefix+%3A+%3C" .. encodedNpUrl .. "%3E%0D%0Aselect+%3FG+%3FS+%3FP+%3FO+where+{%0D%0A++{graph+%3FG+{%3A+a+np%3ANanopublication}}+union%0D%0A++{graph+%3FH+{%3A+a+np%3ANanopublication+{%3A+np%3AhasAssertion+%3FG}+union+{%3A+np%3AhasProvenance+%3FG}+union+{%3A+np%3AhasPublicationInfo+%3FG}}}%0D%0A++graph+%3FG+{%3FS+%3FP+%3FO}%0D%0A}&format=text%2Fcsv&timeout=0"
    local npResponse = http.request_batch({{"GET", getUrl, response_body_bytes = 10000000}})
  end
end
