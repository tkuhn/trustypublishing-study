#!/bin/bash
#
# Usage:
#
# $ ./preprocess-data.sh trustypublishing-test
# $ ./preprocess-data.sh sparql-test
#

if [ "$1" == "" ]; then
  echo "Use argument: 'trustypublishing-test' or 'sparql-test'"
  exit 1
fi

( echo "TIME RECEIVED,URL,STATUS CODE,ROUND-TRIP DURATION,CONTENT LENGTH" ;
  less data/$1.csv.gz \
    | sed 's/Dublin, IE/Dublin IE/g' \
    | sed 's/"//g' \
    | grep "Amazon EU West (Dublin IE)" \
    | egrep ",GET," \
    | sed -e 's/^[0-9\-]* \([0-9]*\):\([0-9]*\):\([0-9\.]*\)+00:00,/\1,\2,\3,/' \
    | awk -F',' '{ t = $1*3600 + $2*60 + $3; printf("%0.10g,%s,%d,%0.10g,%d\n", t, $5, $8, $11, $17); }' \
) | gzip -f \
  > data/$1-processed.csv.gz
