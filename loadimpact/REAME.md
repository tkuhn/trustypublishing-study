There is a problem with the trustypublishing data (long sequence of NULL characters):

    $ less data/trustypublishing-test.csv.gz | grep "http://np.inn.ac/RA3U065XmHVDI7qj3NwMdGiZ4AbeyNr9O-gEzg3FUsFPc" | od -bc
