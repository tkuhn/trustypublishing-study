#!/usr/bin/r
#
# to run this script type:
# r eval.R

library(fields)

sparqldata = read.csv("data/sparql-test-processed.csv.gz", header=TRUE)
trustydata = read.csv("data/trustypublishing-test-processed.csv.gz", header=TRUE)

cat("sparql-test\n")
cat("count: ", nrow(sparqldata), "\n")
cat("average: ", mean(sparqldata$"ROUND.TRIP.DURATION"), "\n")

cat("trustypublishing-test\n")
cat("count: ", nrow(trustydata), "\n")
cat("average: ", mean(trustydata$"ROUND.TRIP.DURATION"), "\n")

sDataY = sparqldata$"ROUND.TRIP.DURATION" / 1000
sDataX = sparqldata$"TIME.RECEIVED" - sDataY - min(sparqldata$"TIME.RECEIVED" - sDataY)
tDataY = trustydata$"ROUND.TRIP.DURATION" / 1000
tDataX = trustydata$"TIME.RECEIVED" - tDataY - min(trustydata$"TIME.RECEIVED" - tDataY)

#png("data/plot.png", width=800, height=600, units='px')
pdf("data/plot.pdf", width=12, height=6)
par(ps=16, las=1)
plot(0, xlim=c(0, 300), ylim=c(0.05, 130), log="y", xaxs="i", yaxs="i", yaxt="n", xaxt="n", xlab="time from start of test in seconds", ylab="response time in seconds")
bplot.xy(sDataX, sDataY, N=25, col=rgb(0.7,0,0,0.5), add=TRUE, yaxt="n", pars=list(outpch=1,outcol=rgb(0.7,0.4,0.4,0.3),outcex=0.5))
bplot.xy(tDataX, tDataY, N=25, col=rgb(0,0,0.7,0.5), add=TRUE, yaxt="n", pars=list(outpch=4,outcol=rgb(0.4,0.4,0.7,0.3),outcex=0.5))

axis(2,at=c(0.1,1,10,100),labels=c(0.1,1,10,100))
axis(side=3, at=seq(0, 300, by=60), labels=seq(0, 100, by=20))
mtext("number of clients accessing the service in parallel", side=3, line=3)

legend(3, 125, "Virtuoso triple store with SPARQL endpoint",
lty=0, pch=15, col=rgb(0.7,0,0,0.5), bty="n")

legend(3, 75, "nanopublication server",
lty=0, pch=15, col=rgb(0,0,0.7,0.5), bty="n")

