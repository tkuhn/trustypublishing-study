#!/bin/bash
#
# Usage:
#
# $ ./make_monitor_tables.sh data/logs/zurich/nanopub-monitor.log.0b > tables/zurich-monitor.csv
# $ ./make_monitor_tables.sh data/logs/ottawa/nanopub-monitor.log.0b > tables/ottawa-monitor.csv
#

cat "$@" \
  | grep " ch.tkuhn.nanopub.monitor.ServerData - Test result: " \
  | sed -r 's/^.* [^ ]*([0-9][0-9])T([0-9][0-9]):([0-9][0-9]):([0-9][0-9])\.[^ ]* [^ ]* - Test result: ([^ ]*) ([^ ]*) ([^ ]*)( ([^ ]*)ms)?/\1,\2,\3,\4,\5,\6,\7,\9/'

