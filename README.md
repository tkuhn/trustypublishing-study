Trusty Publishing Study
=======================

This repo contains the files that were used to study the system described in
this paper:

- Tobias Kuhn, Christine Chichester, Michel Dumontier, and Michael Krauthammer.
  Publishing without Publishers: a Decentralized Approach to Dissemination,
  Retrieval, and Archiving of Data. In Proceedings of the 14th International
  Semantic Web Conference (ISWC). Springer, 2015.
  [[PDF](http://arxiv.org/pdf/1411.2749.pdf)]

See also this repository of an extension of this study for an extended journal article:

- [trustypublishingx-study](https://bitbucket.org/tkuhn/trustypublishingx-study/)

