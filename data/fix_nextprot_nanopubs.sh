#!/bin/bash
#
# Usage:
# $ ./fix_nextprot_nanopubs.sh 00001_01000

cd "$( dirname "${BASH_SOURCE[0]}" )"

cat indexing/nextprot/nextprot_$1/nextprot_$1.trig.gz \
  | gunzip \
  | sed 's/^\(@prefix sub: .*\)#> .$/\1.> ./' \
  | sed 's/&amp;amp;#60;/%3C/g' \
  | sed 's/&amp;amp;#62;/%3E/g' \
  | gzip \
  > indexing/nextprot/nextprot_$1/nextprot_${1}_mod.trig.gz

../nanopub-java/scripts/FixTrustyNanopub.sh indexing/nextprot/nextprot_$1/nextprot_${1}_mod.trig.gz
