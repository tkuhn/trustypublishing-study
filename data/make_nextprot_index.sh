cd "$( dirname "${BASH_SOURCE[0]}" )"

../nanopub-java/scripts/MakeIndex.sh \
  -o indexing/nextprot/index.nextprot.trig.gz \
  -t "Nanopubs converted from neXtProt protein data (preliminary)" \
  -d "These nanopubs were automatically extracted from the neXtProt dataset. This set of nanopublications is preliminary." \
  -a http://sourceforge.net/projects/nextprot2rdf/files/data/nextprot/releases/2014-09/ \
  -a http://www.nextprot.org/ \
  -c 0000-0001-6710-1373 \
  indexing/nextprot/nextprot_*/fixed.*.trig.gz
