cd "$( dirname "${BASH_SOURCE[0]}" )"

../nanopub-java/scripts/MakeIndex.sh \
  -o indexing/disgenet/index.nanopublications_v2.1.0.0.trig.gz \
  -t "Nanopubs extracted from DisGeNET v2.1.0.0" \
  -d "These nanopubs were automatically extracted from the DisGeNET dataset." \
  -a http://biorxiv.org/content/early/2014/10/16/010397 \
  -c 0000-0003-0169-8159 \
  indexing/disgenet/nanopublications_v2.1.0.0.trig.gz

