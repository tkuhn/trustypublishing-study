# Load GeneRIF/AIDA:

nanopub-java/scripts/GetNanopub.sh -i -o data/nanopubs/generif-aida.index.trig.gz RAY_lQruuagCYtAcKAPptkY7EpITwZeUilGHsWGm9ZWNI
nanopub-java/scripts/GetNanopub.sh -c -o data/nanopubs/generif-aida.trig.gz RAY_lQruuagCYtAcKAPptkY7EpITwZeUilGHsWGm9ZWNI

# Load OpenBEL corpora 1.0:

nanopub-java/scripts/GetNanopub.sh -i -o data/nanopubs/openbel-1.0.index.trig.gz RACy0I4f_wr62Ol7BhnD5EkJU6Glf-wp0oPbDbyve7P6o
nanopub-java/scripts/GetNanopub.sh -c -o data/nanopubs/openbel-1.0.trig.gz RACy0I4f_wr62Ol7BhnD5EkJU6Glf-wp0oPbDbyve7P6o

# Load OpenBEL corpora 20131211:

nanopub-java/scripts/GetNanopub.sh -i -o data/nanopubs/openbel-20131211.index.trig.gz RAR5dwELYLKGSfrOclnWhjOj-2nGZN_8BW1JjxwFZINHw
nanopub-java/scripts/GetNanopub.sh -c -o data/nanopubs/openbel-20131211.trig.gz RAR5dwELYLKGSfrOclnWhjOj-2nGZN_8BW1JjxwFZINHw

# Load DisGeNET v2.1.0.0:

nanopub-java/scripts/GetNanopub.sh -i -o data/nanopubs/disgenet-v2.1.0.0.index.trig.gz RAXy332hxqHPKpmvPc-wqJA7kgWiWa-QA0DIpr29LIG0Q
nanopub-java/scripts/GetNanopub.sh -c -o data/nanopubs/disgenet-v2.1.0.0.trig.gz RAXy332hxqHPKpmvPc-wqJA7kgWiWa-QA0DIpr29LIG0Q

# Load neXtProt:

nanopub-java/scripts/GetNanopub.sh -i -o data/nanopubs/nextprot.index.trig.gz RAXFlG04YMi1A5su7oF6emA8mSp6HwyS3mFTVYreDeZRg
nanopub-java/scripts/GetNanopub.sh -c -o data/nanopubs/nextprot.trig.gz RAXFlG04YMi1A5su7oF6emA8mSp6HwyS3mFTVYreDeZRg
