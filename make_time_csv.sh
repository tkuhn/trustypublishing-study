#!/bin/bash

cat results/nanopub-times.txt | grep "Time to second server is" | sed -r 's/^.* ([0-9]*) minutes: ([0-9]*)$/\1,\2/' > results/times2.csv
cat results/nanopub-times.txt | grep "Time to third server is" | sed -r 's/^.* ([0-9]*) minutes: ([0-9]*)$/\1,\2/' > results/times3.csv
