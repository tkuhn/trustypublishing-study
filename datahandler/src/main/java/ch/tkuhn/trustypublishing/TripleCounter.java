package ch.tkuhn.trustypublishing;

import java.io.File;

import org.nanopub.MultiNanopubRdfHandler;
import org.nanopub.Nanopub;

public class TripleCounter {

	public static void main(String[] args) throws Exception {
		for (String arg : args) {
			System.err.println(arg);
			TripleCounter obj = new TripleCounter(new File(arg));
			obj.run();
			System.err.println("Triple count: " + obj.getTripleCount());
		}
	}

	private File file;
	private int tripleCount = 0;

	private TripleCounter(File file) {
		this.file = file;
	}

	private void run() throws Exception {
		MultiNanopubRdfHandler.process(file, new MultiNanopubRdfHandler.NanopubHandler() {

			@Override
			public void handleNanopub(Nanopub np) {
				tripleCount += np.getHead().size();
				tripleCount += np.getAssertion().size();
				tripleCount += np.getProvenance().size();
				tripleCount += np.getPubinfo().size();
			}

		});
	}

	public int getTripleCount() {
		return tripleCount;
	}

}
