package ch.tkuhn.trustypublishing;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;

import net.trustyuri.TrustyUriUtils;

import org.nanopub.MultiNanopubRdfHandler;
import org.nanopub.Nanopub;

public class DataHandler {

	public static final int datasetCount = 4;

	public static final int DISGENET = 0;
	public static final int GENERIF = 1;
	public static final int OPENBEL = 2;
	public static final int NEXTPROT = 3;

	public static final String[] uriPrefixes = new String[datasetCount];

	static {
		uriPrefixes[DISGENET] = "http://rdf.disgenet.org/";
		uriPrefixes[GENERIF] = "http://krauthammerlab.med.yale.edu/nanopub/GeneRIF";
		uriPrefixes[OPENBEL] = "http://www.tkuhn.ch/bel2nanopub/";
		uriPrefixes[NEXTPROT] = "http://www.nextprot.org/";
	}

	public static final String indexPrefix = "http://np.inn.ac/";
	public static final String examplePrefix = "http://example.org/";

	public static final String[] npFileNames = new String[datasetCount];

	static {
		npFileNames[DISGENET] = "disgenet-v2.1.0.0";
		npFileNames[GENERIF] = "generif-aida";
		npFileNames[OPENBEL] = "openbel-1.0 openbel-20131211";
		npFileNames[NEXTPROT] = "nextprot";
	}

	public static final int locationCount = 4;

	public static final int LOCAL = 0;
	public static final int ZURICH = 1;
	public static final int OTTAWA = 2;
	public static final int NEWHAVEN = 3;

	public static final int[] physicalLocations = new int[] { ZURICH, OTTAWA, NEWHAVEN };

	public static final String[] locationNames = new String[locationCount];

	static {
		locationNames[LOCAL] = "local";
		locationNames[ZURICH] = "zurich";
		locationNames[OTTAWA] = "ottawa";
		locationNames[NEWHAVEN] = "newhaven";
	}

	public static final String zurichUrl = "http://np.inn.ac/";
	public static final String ottawaUrl = "http://s1.semanticscience.org:8080/nanopub-server/";
	public static final String newhavenUrl = "http://ristretto.med.yale.edu:8080/nanopub-server/";

	public static final Map<String,Integer> serverLocations = new HashMap<>();

	static {
		serverLocations.put(zurichUrl, ZURICH);
		serverLocations.put(ottawaUrl, OTTAWA);
		serverLocations.put(newhavenUrl, NEWHAVEN);
	}

	public static final double zurichStartTime = 24.0*30 + 22 + 33/60.0;
	public static final double ottawaStartTime = 24.0*30 + 17 + 24/60.0 + 1/3600.0;
	public static final double newhavenStartTime = 24.0*30 + 17 + 33/60.0;

	public static final Map<Integer,Double> startTimes = new HashMap<>();

	static {
		startTimes.put(ZURICH, zurichStartTime);
		startTimes.put(OTTAWA, ottawaStartTime);
		startTimes.put(NEWHAVEN, newhavenStartTime);
	}

	public static void main(String[] args) throws Exception {
		DataHandler obj = new DataHandler();
		obj.run();
	}

	private float dimFactor = 20.0f;
	private int barHeight = 50;
	private int hourMarkWidth = 1;
	private int timeStepsPerHour = 60 * 2;  // set to 6 for load intensity diagram
	private float endTimeInHours = 13.4f;
	private int timeSteps;

	private Map<String,Integer> indexMap;
	private Map<Long,Boolean> seen;
	private int[][] trafficMatrix;
	private short[][][][] dataFlow;
	private Map<Long,Double[]> nanopubTimes;
	private int[][] loadIntensity;

	private DataHandler() {
	}

	private void run() throws Exception {
		init();
		createIndexMap();
		processLogFiles();
		showResults();
	}

	private void init() {
		indexMap = new HashMap<>();
		timeSteps = (int) (endTimeInHours * timeStepsPerHour);
		trafficMatrix = new int[locationCount][locationCount];
		dataFlow = new short[locationCount][locationCount][locationCount][timeSteps+1];
		nanopubTimes = new HashMap<>();
		loadIntensity = new int[3][timeSteps+1];
	}

	private void showResults() throws Exception {
		BufferedWriter wr;
		wr = new BufferedWriter(new FileWriter("../results/traffic.txt"));
		for (int from = 0 ; from < locationCount ; from++) {
			String fromName = locationNames[from];
			for (int to = 0 ; to < locationCount ; to++) {
				if (trafficMatrix[from][to] == 0) continue;
				String toName = locationNames[to];
				wr.write("From " + fromName + " to " + toName + ": " + trafficMatrix[from][to] + "\n");
			}
		}
		wr.close();
		int imageHeight = (locationCount * (locationCount-1) * barHeight) - barHeight;
		BufferedImage image = new BufferedImage(timeSteps, imageHeight, BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics = (Graphics2D) image.getGraphics();
		float maxFlow = 0;
		for (int t = 0 ; t < timeSteps ; t++) {
			for (int from = 0 ; from < locationCount ; from++) {
				for (int to = 1 ; to < locationCount ; to++) {
					if (from == to) continue;
					int yPos = 0;
					if (from == ZURICH) {
						yPos = barHeight;
					} else if (from == NEWHAVEN) {
						yPos = 2 * barHeight;
						if (to == ZURICH) yPos = barHeight;
					} else if (from == OTTAWA) {
						yPos = 2 * barHeight;
					}
					if (to == NEWHAVEN) {
						yPos += 4 * barHeight;
					} else if (to == OTTAWA) {
						yPos += 8 * barHeight;
					}
					float flow = 0;
					flow += dataFlow[from][to][ZURICH][t];
					flow += dataFlow[from][to][NEWHAVEN][t];
					flow += dataFlow[from][to][OTTAWA][t];
					if (flow > maxFlow) maxFlow = flow;
					graphics.setColor(getColor(from, to, t));
					graphics.fillRect(t, yPos, 1, barHeight);
				}
			}
			if (t % timeStepsPerHour == hourMarkWidth) {
				graphics.setColor(Color.white);
				graphics.fillRect(t-hourMarkWidth, 0, hourMarkWidth, imageHeight);
			}
		}
		ImageIO.write(image, "png", new File("../results/dataflow.png"));
		System.out.println("Maximum flow in time slot: " + maxFlow);

		double totalTime2 = 0.0;
		double totalTime3 = 0.0;
		Map<Integer,Integer> hist2 = new HashMap<>();
		Map<Integer,Integer> hist3 = new HashMap<>();
		int histMax = 0;
		int count = 0;
		for (long npCode : nanopubTimes.keySet()) {
			Double[] times = nanopubTimes.get(npCode);
			if (times[2] == null) continue;
			Arrays.sort(times);
			double t2 = times[1] - times[0];
			double t3 = times[2] - times[0];
			totalTime2 += t2;
			totalTime3 += t3;
			int i2 = (int) (t2 * 60);
			int i3 = (int) (t3 * 60);
			if (!hist2.containsKey(i2)) {
				hist2.put(i2, 1);
			} else {
				hist2.put(i2, hist2.get(i2)+1);
			}
			if (i2 > histMax) histMax = i2;
			if (!hist3.containsKey(i3)) {
				hist3.put(i3, 1);
			} else {
				hist3.put(i3, hist3.get(i3)+1);
			}
			if (i3 > histMax) histMax = i3;

			for (int s = 0 ; s < 3 ; s++) {
				int timeSlot = (int) (times[s] * timeStepsPerHour);
				loadIntensity[s][timeSlot]++;
			}

			count++;
		}
		wr = new BufferedWriter(new FileWriter("../results/nanopub-times.txt"));
		if (count != nanopubTimes.size()) {
			wr.write("WARNING: Time data is not complete!\n\n");
		}
		wr.write("Number of nanopubs: " + count + "\n");
		wr.write("Total time to second server in hours: " + totalTime2 + "\n");
		wr.write("Total time to third server in hours: " + totalTime3 + "\n");
		wr.write("Average time to second server in minutes: " + ((totalTime2*60) / count) + "\n");
		wr.write("Average time to third server in minutes: " + ((totalTime3*60) / count) + "\n");
		wr.write("\n");
		for (int h = 0 ; h < histMax+1 ; h++) {
			int v = 0;
			if (hist2.containsKey(h)) v = hist2.get(h);
			wr.write("Time to second server is " + h + " minutes: " + v + "\n");
		}
		wr.write("\n");
		for (int h = 0 ; h < histMax+1 ; h++) {
			int v = 0;
			if (hist3.containsKey(h)) v = hist3.get(h);
			wr.write("Time to third server is " + h + " minutes: " + v + "\n");
		}
		wr.close();
		wr = new BufferedWriter(new FileWriter("../results/load-intensity.csv"));
		for (int t = 0 ; t < timeSteps+1 ; t++) {
			for (int i = 0 ; i < 3 ; i++) {
				wr.write(loadIntensity[i][t] + "");
				if (i < 2) wr.write(",");
			}
			wr.write("\n");
		}
		wr.close();
	}

	private Color getColor(int from, int to, int t) {
		float g = dataFlow[from][to][ZURICH][t];
		int green = 0;
		if (g > 0) green = 64 + (int) (g / dimFactor);
		if (green > 255) green = 255;
		float r = dataFlow[from][to][NEWHAVEN][t];
		int red = 0;
		if (r > 0) red = 64 + (int) (r / dimFactor);
		if (red > 255) red = 255;
		float b = dataFlow[from][to][OTTAWA][t];
		int blue = 0;
		if (b > 0) blue = 64 + (int) (b / dimFactor);
		if (blue > 255) blue = 255;
		return new Color(red, green, blue);
	}

	private void processLogFiles() throws Exception {
		System.err.println("Processing log files...");
		for (int location : physicalLocations) {
			String locationName = locationNames[location];
			File file = new File("../data/logs/" + locationName + "/nanopub-server.large.log");
			processLogFile(file, location);
		}
	}

	private static final String nploadLogPattern = "^[^ ]* 2014-10-([0-9]*)T([0-9]*):([0-9]*):([0-9]*).([0-9]*).* ([^ ]*)$";

	private void processLogFile(File file, int location) throws Exception {
		System.err.println("Processing log file " + file);
		seen = new HashMap<>();
		BufferedReader br = new BufferedReader(new FileReader(file));
		int count = 0;
		int downloadLocation = -1;
		String line;
		while ((line = br.readLine()) != null) {
			if (count % 1000 == 0) System.err.print(count + "\r");
			if (line.contains("Checking if there are new nanopubs at ")) {
				String url = line.replaceFirst("^.* ([^ ]*)$", "$1");
				downloadLocation = serverLocations.get(url);
			} else if (line.contains("Nanopub loaded: ")) {
				double day = Double.parseDouble(line.replaceFirst(nploadLogPattern, "$1"));
				double hour = Double.parseDouble(line.replaceFirst(nploadLogPattern, "$2"));
				double min = Double.parseDouble(line.replaceFirst(nploadLogPattern, "$3"));
				double sec = Double.parseDouble(line.replaceFirst(nploadLogPattern, "$4"));
				double milli = Double.parseDouble(line.replaceFirst(nploadLogPattern, "$5"));
				String npUri = line.replaceFirst(nploadLogPattern, "$6");
				if (npUri.startsWith(examplePrefix)) continue;
				long npCode = getCode(npUri);
				if (seen.containsKey(npCode)) continue;
				seen.put(npCode, true);
				double time = day*24 + hour + min/60 + sec/3600 + milli/3600000;
				time = time - startTimes.get(location);
				if (time < 0) continue;
				int from = downloadLocation;
				int dataOrigin = getLocation(getDataset(npUri));
				if (dataOrigin == location) from = LOCAL;
				count++;
				processEntry(time, from, location, dataOrigin, npCode);
			}
		}
		System.err.println(count);
		br.close();
	}

	private void processEntry(double time, int from, int to, int dataOrigin, long npCode) {
		trafficMatrix[from][to]++;
		int timeSlot = (int) (time * timeStepsPerHour);
		dataFlow[from][to][dataOrigin][timeSlot]++;
		if (!nanopubTimes.containsKey(npCode)) {
			nanopubTimes.put(npCode, new Double[locationCount-1]);
		}
		Double[] d = nanopubTimes.get(npCode);
		for (int i = 0 ; i < locationCount ; i++) {
			if (i == locationCount-1) {
				throw new RuntimeException("Error. Nanopub was loaded more often than there are servers");
			}
			if (d[i] == null) {
				d[i] = time;
				break;
			}
		}
	}

	private void createIndexMap() throws Exception {
		System.err.println("Creating index map...");
		for (int dataset = 0 ; dataset < datasetCount ; dataset++) {
			for (String s : npFileNames[dataset].split(" ")) {
				File file = new File("../data/nanopubs/" + s + ".index.trig.gz");
				processIndexFile(file, dataset);
			}
		}
	}

	private void processIndexFile(File file, final int dataset) throws Exception {
		System.err.println("Processing nanopub index file " + file);
		MultiNanopubRdfHandler.process(file, new MultiNanopubRdfHandler.NanopubHandler() {
			
			@Override
			public void handleNanopub(Nanopub np) {
				String uri = np.getUri().toString();
				if (indexMap.containsKey(uri)) {
					throw new RuntimeException("Duplicated URI: " + uri);
				}
				indexMap.put(uri, dataset);
			}

		});
	}

	private int getDataset(String uri) {
		for (int d = 0 ; d < datasetCount ; d++) {
			if (uri.startsWith(uriPrefixes[d])) {
				return d;
			}
		}
		if (indexMap.containsKey(uri)) {
			return indexMap.get(uri);
		}
		throw new RuntimeException("Unknown URI: " + uri);
	}

	private int getLocation(int dataset) {
		if (dataset == DISGENET) return ZURICH;
		if (dataset == GENERIF || dataset == OPENBEL) return NEWHAVEN;
		if (dataset == NEXTPROT) return OTTAWA;
		throw new RuntimeException("Invalid dataset: " + dataset);
	}

	private static long getCode(String npUri) {
		// Calculate long integer value for hash (should be unique fo 5M entries)
		String shortCode = TrustyUriUtils.getArtifactCode(npUri).substring(2, 14);
		shortCode = shortCode.replace('-', '+').replace('_', '/');
		byte[] bytes = DatatypeConverter.parseBase64Binary(shortCode);
		long code = 0;
		for (int i = 0 ; i < 8 ; i++) {
			code = 256*code + (bytes[i] - Byte.MIN_VALUE);
		}
		return code;
	}

}
